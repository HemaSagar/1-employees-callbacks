const fs = require('fs')
const path = require('path')

const dataFilePath = path.join(__dirname, 'data.json');
fs.readFile(dataFilePath, 'utf-8', (err, data) => {
    if(err){
        console.error(`error occured while reading file data.json ${err.message}`);
    }
    else{
        console.log(`employees data read succesfully from data.json`);
        const employees = JSON.parse(data);
        

        mainFunction(employees);
    }

});

function mainFunction(employeesData){

    // 1. Retrieve data for ids : [2, 13, 23].

    const requiredIds = [2,13,23];
    const requiredEmployees = employeesData['employees'].filter((employee) => {
        return requiredIds.includes(employee.id);
    })
    const outputJson1 = 'jsonOutput1.json';
    const outputJson1Path = path.join(__dirname,outputJson1);

    fs.writeFile(outputJson1Path, JSON.stringify(requiredEmployees,null,2), (err) => {
        if(err){
            console.error(`error while writing data to ${outputJson1} file ${err.message}`);
        }
        else{
            console.log(`[1,13,23] employees data written succesfully to ${outputJson1} file`);
            groupCompanies();
        }
    })

    function groupCompanies(){
        const companiesGrouped = employeesData['employees'].reduce((groupedData, employee) => {
            if(groupedData[employee['company']]){
                groupedData[employee['company']].push({...employee});
            }
            else{
                groupedData[employee['company']] = [{...employee}];
            }
            return groupedData;
        },{});

        const outputJson2 = 'jsonOutput2.json';
        const outputJson2Path = path.join(__dirname,outputJson2);

        fs.writeFile(outputJson2Path, JSON.stringify(companiesGrouped, null, 2), (err)=>{
            if(err){
                console.error(`Error occurres while writing contents to ${outputJson2} file`);
            }
            else{
                console.log(`companies groupes data written to ${outputJson2} file successfully`);

            }
        })
    }


}